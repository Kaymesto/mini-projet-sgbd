package fr.afpa.control;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.model.ManageTable;

public interface ControlRequest {
	
	
	public static int requestControl(String request) {
		String strverif = "";
		String nomFichier = "";
		String nomChamp = "";
		int verif = 0;
		int nbligne = 0;
		if (request.split(" ")[0].equalsIgnoreCase("create")){
		Pattern p = Pattern.compile("(?i)(create table) [A-Za-z_\\d]{1,25}\\(([A-Za-z_\\d]{1,25},)*[A-Za-z_\\d]{1,25}\\);");
		Matcher m = p.matcher(request);
		if(m.matches()) {
			ManageTable.createTable(request);
			verif = 1;
						}
		else {
			System.out.println("Merci de vérifier votre syntaxe pour la commande create.");
			verif = 11;
		}
// insert into listePersonne VALUES(Elkadiri,Ebrahim,23,Homme);
		}
		
		else 	if (request.split(" ")[0].equalsIgnoreCase("insert")){
			Pattern p = Pattern.compile("(?i)(insert into) [A-Za-z_\\d]{1,25} (values)\\(([A-Za-z_\\d]{1,25},)*[A-Za-z_\\d]{1,25}\\);");
			Matcher m = p.matcher(request);
			if(m.matches()) {
				
				
			verif = 2;		}
			else {
				verif = 21;
			}
			if (verif == 2){
				nomFichier = request.split(" ")[2].split(" ")[0];
				strverif = request.split("\\(")[1];
				strverif = request.split("\\)")[0];
				nbligne = strverif.split(",").length;
				ManageTable.insertionFichier (nomFichier, nbligne,strverif);
			}
			
		} // Select nom FROM listePersonne;
		
		else 	if (request.toUpperCase().contains("SELECT")){
		
			if (request.toUpperCase().contains("SELECT*FROM") && request.toUpperCase().contains("ASC;") != true && request.toUpperCase().contains("DESC;") != true){
				nomChamp = request.split(" ")[1].split(" ")[0];
				nomFichier = request.split(" ")[1].split(";")[0];
				nbligne = strverif.split(",").length;
			ManageTable.affichageComplet(nomFichier, nbligne);
			
			}
			else if (request.split(" ")[2].equalsIgnoreCase("from") == true){
			nomFichier = request.split(" ")[3].split(";")[0];
			nbligne = strverif.split(",").length;
			nomChamp = request.split(" ")[1].split(" ")[0];
			Pattern p = Pattern.compile("(?i)(select) [A-Za-z_\\d]{1,25} (from) [A-Za-z_\\d]{1,25};");
			Matcher m = p.matcher(request);

			if(m.matches()) {
			ManageTable.selectFrom(nomFichier, nbligne, nomChamp);
			}			

			verif = 3;
		}
			else if (request.split(" ")[5].equalsIgnoreCase("asc;") == true){
				nomFichier = request.split(" ")[1].split(" ")[0];
				nomChamp = request.split(" ")[4].split(" ")[0];
				ManageTable.triCroissant(nomFichier, nomChamp);
		}
			else if (request.split(" ")[5].equalsIgnoreCase("desc;") == true){
				nomFichier = request.split(" ")[1].split(" ")[0];
				nomChamp = request.split(" ")[4].split(" ")[0];
				ManageTable.triDecroissant(nomFichier, nomChamp);
		}
		
		}
		
		else 	if (request.split(" ")[0].equalsIgnoreCase("update")){
			Pattern p = Pattern.compile("(?i)(update) [A-Za-z_\\d]{1,25} (set) [A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}';"); 
			Matcher m = p.matcher(request); 
			if(m.matches()) { 
				 
				ManageTable.updateColumn(request); 
				verif = 4; 
				} 
			else { 
				System.out.println("Merci de v�rifier votre syntaxe pour la commande update."); 
				verif = 41; 
			} 
			Pattern p1 = Pattern.compile("(?i)(update) [A-Za-z_\\d]{1,25} (set) ([A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}',)*[A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}';"); 
			Matcher m1 = p1.matcher(request); 
			if(m1.matches()) { 
				 
				ManageTable.updateManyColumn(request); 
				verif = 4; 
				} 
			else { 
				System.out.println("Merci de v�rifier votre syntaxe pour la commande update."); 
				verif = 42; 
			} 
			Pattern p2 = Pattern.compile("(?i)(update) [A-Za-z_\\d]{1,25} (set) [A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}' (WHERE) [A-Za-z_\\d]{1,25}='[A-Za-z_\\d]{1,25}';"); 
			Matcher m2 = p2.matcher(request); 
			if(m2.matches()) { 
				 
				ManageTable.updateOneValue(request); 
				verif = 4; 
				} 
			else { 
				System.out.println("Merci de v�rifier votre syntaxe pour la commande update."); 
				verif = 43; 
			}
			
			verif = 4;
		}
		else if (request.split(" ")[0].equalsIgnoreCase("help")) {
			System.out.println("Voici les différentes commandes existantes avec leur syntaxe définie :"
					+ "\nCREATE TABLE nomTable(colonne1, colonne2,colonne3); : qui permet de créer un fichier avec le nombre de colonnes désirées."
					+ "\nINSERT INTO nomTable VALUES('valeur 1', 'valeur 2'); : qui permet d'ajouter des valeurs à votre table déja créée."
					+ "\nSELECT * FROM nomTable; : qui permet l'affichage de votre table."
					+ "\nSELECT nom_du_champ FROM nomTable; : qui permet l'affichage de la colonne de votre table."
					+ "\nSELECT nom_du_champ,nom_du_champ1 from nomTable; : qui permet l'affichage des colonnes désirées."
					+ "\nSELECT*FROM nomTable Order by nom_du_champ ASC(or DESC); : qui permet le tri d'une colonne par ordre croissant ou décroissant."
					+ "\nUPDATE nomTable SET nom_du_champ = 'nouvelle valeur'; : qui permet de modifier toutes les valeurs d'une colonne."
					+ "\nUPDATE nomTable SET nom_du_champ = 'nouvelle valeur' ,nom_du_champ1 = 'nouvelle valeur1'; : qui permet de remplacer une valeur par une nouvelle."
					+ "\nUPDATE nomTable SET nom_du_champ = 'nouvelle valeur' WHERE nom_du_champ = 'anciennevaleur'; : qui permet de modifier le nom de la colonne de votre choix.");
			System.out.println();}
		
		else { System.out.println("Merci de vérifier votre commande ou taper help pour voir les commandes possibles.");}
	
		// Select*From listePersonne Order by nom asc;
	
	

	
		
				return verif;

	}
}
