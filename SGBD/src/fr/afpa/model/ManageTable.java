package fr.afpa.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;

public abstract class ManageTable {
	boolean connected = false;
	static ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");

	public static boolean createTable(String str) {
		boolean verif = false;
		int i;
		String line = "";
		Object a = new Object();

		String name = str.split(" ")[2].split("\\(")[0];
		File f = new File(rb.getString("path")+name+".cda");
		FileWriter fw = null;
		BufferedWriter bw = null;
		String columns = str.split("\\(")[1].split("\\)")[0];
		line = "" + columns.split(",").length + ";";
		for (i = 0; i < columns.split(",").length; i++) {
			line += columns.split(",")[i] + ";";
		}
		try {
			fw = new FileWriter(rb.getString("path") + name + ".cda");
			bw = new BufferedWriter(fw);
			bw.write(line);
			System.out.println(line);
		} catch (Exception e) {
			System.out.println("Erreur" + e);
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return verif;

	}

	public static boolean read() {
		return false;

	}

	public static boolean updateColumn(String str) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		boolean verif = false;
		String name = str.split(" ")[1];
		String newValue = str.split("'")[1].split("'")[0];
		String nomChamp = str.split(" ")[3].split("=")[0];
		String liste ="";
		System.out.println(newValue);
		FileReader fr = null;
		BufferedReader br = null;
		String[] tab;
		String val ="";
		int nbColumn = 0;
		try {
			fr = new FileReader(rb.getString("path") + name + ".cda");
			br = new BufferedReader(fr);
			val = br.readLine();
		} catch (Exception e) {
			System.out.println("Erreur" + e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}			
		tab = val.split(";");
		nbColumn = Integer.parseInt(tab[0]);

		int compteur =0;
		for (int i =0; i <tab.length; i++) {
			if (tab[i].contentEquals(nomChamp) == true) {
				break;
			}
			else {
				compteur++;
			}
		}
		
		for (int i = compteur+nbColumn; i<tab.length; i+= nbColumn) {
			tab[i] = newValue;
		}
		for (int i =0; i<tab.length; i++) {
			liste += tab[i]+";";
		}
		//update personne set prenom='Ebrahim';
		File f = new File(rb.getString("path") + name + ".cda");
		
		FileWriter fw = null;
		BufferedWriter bw = null;
		System.out.println(liste);
		try {
			fw = new FileWriter(rb.getString("path") + name + ".cda");
			bw = new BufferedWriter(fw);
			bw.write(liste);
			verif = true;

		}
		catch(Exception e) {
			System.out.println("Erreur" + e);
		}
		finally{
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		return verif;
	}

	public static boolean updateManyColumn(String str) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		boolean verif = false;
		return verif;

	}

	public static boolean updateOneValue(String str) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		boolean verif = false;
		String name = str.split(" ")[1];
		String newValue = str.split("'")[1].split("'")[0];
		String nomChamp = str.split(" ")[3].split("=")[0];
		String liste ="";
		System.out.println(newValue);
		FileReader fr = null;
		BufferedReader br = null;
		String[] tab;
		String val ="";
		int nbColumn = 0;
		try {
			fr = new FileReader(rb.getString("path") + name + ".cda");
			br = new BufferedReader(fr);
			val = br.readLine();
		} catch (Exception e) {
			System.out.println("Erreur" + e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return verif;

	}

	public static boolean insertionFichier(String str, int nbligne, String values) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		boolean retour = false;
		String infosFichier = "";
		FileReader fr = null;
		BufferedReader br = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fr = new FileReader(rb.getString("path") + str + ".cda");
			br = new BufferedReader(fr);
			infosFichier = br.readLine();
			if (nbligne == Integer.parseInt(infosFichier.split(";")[0])) {
				retour = true;
			}
		} catch (Exception e) {
			System.out.println("Le fichier n'a pas ete trouve ou n'existe pas.");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		for (int i = 0; i < values.split(",").length; i++) {
			infosFichier += values.split("\\(")[1].split(",")[i] + ";";
		}
		if (retour == true) {
			try {
				fw = new FileWriter(rb.getString("path") + str + ".cda");
				bw = new BufferedWriter(fw);
				bw.write("" + infosFichier);

			} catch (Exception e) {

			} finally {
				if (bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} // update personne set prenom='ebrahim';
		return retour;
	}

	public static void selectFrom(String nomFichier, int nbligne, String nomChamp) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		String infosFichier = "";
		FileReader fr = null;
		BufferedReader br = null;
		
		try {
			fr = new FileReader (rb.getString("path")+nomFichier+".cda");
			br = new BufferedReader(fr);
			infosFichier = br.readLine();

		}
		catch (Exception e) {
			System.out.println("Le fichier n'a pas ïetïe trouve ou n'existe pas.");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		for (int i =0; i < Integer.parseInt(infosFichier.split(";")[0]); i++) {
			if (nomChamp.equals(infosFichier.split(";")[i])) {
				System.out.println(infosFichier.split(";")[i]);
				for (int j = i + Integer.parseInt(infosFichier.split(";")[0]); j < infosFichier
						.split(";").length; j += Integer.parseInt(infosFichier.split(";")[0])) {
					System.out.println(infosFichier.split(";")[j]);

				}					

				
			}
		}

	}

	// Select nom From listePersonne;

	// insert into listePersonne VALUES(ElKadiri,Ibrahim,23,Homme);
	// insert into listePersonne VALUES(Lohez,Christian,38,Homme);
	// insert into listePersonne VALUES(Fares,Amal,30,Homme);
	// insert into listePersonne VALUES(Meddah,Mohamed,31,Homme);
	// insert into listePersonne VALUES(Benjira,Mohammed,36,Homme);

	public static void affichageComplet(String nomFichier, int nbligne) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		String infosFichier = "";
		FileReader fr = null;
		BufferedReader br = null;

		try {
			fr = new FileReader(rb.getString("path") + nomFichier + ".cda");
			br = new BufferedReader(fr);
			infosFichier = br.readLine();

		} catch (Exception e) {
			System.out.println("Le fichier n'a pas ïetïe trouvïe ou n'existe pas.");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		for (int i = 1; i < infosFichier.split(";").length; i++) {
			System.out.print(infosFichier.split(";")[i] + " ");
			if (i % Integer.parseInt(infosFichier.split(";")[0]) == 0) {
				System.out.println();
			}
		}

	}

	public static void triCroissant(String nomFichier, String nomChamp) {
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config");
		String infosFichier = "";
		FileReader fr = null;
		BufferedReader br = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		String[] tab;
		int nbColonne = 0;
		int compteur = 1;
		int a = 0;

		try {
			fr = new FileReader(rb.getString("path") + nomFichier + ".cda");
			br = new BufferedReader(fr);
			infosFichier = br.readLine();

		} catch (Exception e) {
			System.out.println("Le fichier n'a pas ïetïe trouvïe ou n'existe pas.");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		for (int i =1; i <= Integer.parseInt(infosFichier.split(";")[0]); i++ ) {
			if (infosFichier.split(";")[i].equals(nomChamp) != true){
				a++;
			} else {
				System.out.println(a + " nomCHamp trouve");
				break;
		}}

		tab = new String[((infosFichier.split(";").length / 4) + 1)];
		for (int i = 0; i < tab.length; i++) {
			tab[i] = "";
		}
		tab[0] = infosFichier.split(";")[0];
		for (int i =1; i < infosFichier.split(";").length; i++) {
			

			tab[compteur] += infosFichier.split(";")[i]+";";
			
			if (i % Integer.parseInt(tab[0]) ==0 && i!=0) {
				compteur++;
			}
		}

		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i]);
		}
		System.out.println("Fin premier affichage");
	     Map<String,String> liste = new HashMap<String,String>();
		System.out.println(tab.length);
		for (int i = 1+nbColonne; i<tab.length; i ++) {
			System.out.println(i+"");
			liste.put(tab[i].split(";")[a], tab[i]);
		}
	      Map sortedMap = new TreeMap(liste);
	      Set set2 = sortedMap.entrySet();
	      Iterator iterator2 = set2.iterator();
	      System.out.println("Aprïes le tri: ");
	      int i =1;
	      while(iterator2.hasNext()) {
	         Map.Entry me2 = (Map.Entry)iterator2.next();
	        // System.out.println(me2.getValue());
	         tab[i] = (String) me2.getValue(); 
	         i++;}
	      String listeTrie = "";
	      for (i =0; i<tab.length;i++) {
	    	  System.out.println();
	    	  listeTrie += tab[i]+";";
	      }
	      
	      System.out.println(listeTrie);
	      
	    

		try {
			fw = new FileWriter(rb.getString("path") + nomFichier + ".cda");
			bw = new BufferedWriter(fw);
			bw.write("" + listeTrie);

		} catch (Exception e) {

		} 
		finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
		}
	}

	public static int equals(String a, String b) {
		int retour = 0;
		int longueur = 0;
		if (a.length() > b.length()) {
			longueur = b.length();
		} else {
			longueur = a.length();
		}

		for (int i = 0; i < longueur; i++) {
			if ((int) a.charAt(i) > (int) b.charAt(i)) {
				retour++;
				break;
			} else if ((int) a.charAt(i) < (int) b.charAt(i)) {
				retour--;
				break;
			}
		}

		return retour;
	}
	public static boolean verifDigit (String str) { 
		boolean verif = false; 
		try { 
			Integer.parseInt(str); 
			verif = true; 
		} 
		catch (Exception e) { 
			 
		} 
		return verif; 
	} 
	 
 
 
public static void triDecroissant(String nomFichier, String nomChamp) { 
	ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.configuration.config"); 
	String infosFichier = ""; 
	FileReader fr = null; 
	BufferedReader br = null; 
	FileWriter fw = null; 
	BufferedWriter bw = null; 
	String [] tab; 
	int nbColonne = 0; 
	int compteur =1; 
	int a = 0; 
	 
	try { 
		fr = new FileReader (rb.getString("path")+nomFichier+".cda"); 
		br = new BufferedReader(fr); 
		infosFichier = br.readLine(); 
 
	} 
	catch (Exception e) { 
		System.out.println("Le fichier n'a pas �t� trouv� ou n'existe pas."); 
	} 
	finally { 
		if (br != null) { 
			try { 
				br.close(); 
			} 
			catch (IOException e) { 
				e.printStackTrace(); 
			} 
		} 
	} 
	 
	for (int i =1; i <= Integer.parseInt(infosFichier.split(";")[0]); i++ ) { 
		if (infosFichier.split(";")[i].equals(nomChamp) != true){ 
			a++; 
		} 
		else {break;} 
	} 
	 
	tab = new String [((infosFichier.split(";").length/4)+1)]; 
	for (int i = 0; i <tab.length; i++) { 
		tab[i] = ""; 
	} 
	tab[0] = infosFichier.split(";")[0]; 
	for (int i =1; i < infosFichier.split(";").length; i++) { 
		tab[compteur] += infosFichier.split(";")[i]+";"; 
		 
		if (i % (Integer.parseInt(tab[0])) ==0 && compteur+1 <tab.length) { 
			compteur++; 
		} 
	} 
	System.out.println("Voici l'ancien affichage de votre tableau."); 
	for (int i =0; i < tab.length; i++) { 
		System.out.println(tab[i]); 
	} 
	 
     Map<String,String> liste = new HashMap<String,String>(); 
	for (int i = 1+nbColonne; i<tab.length; i ++) { 
		liste.put(tab[i].split(";")[a], tab[i]); 
	} 
	 
      Map sortedMap = new TreeMap(liste); 
      Set set2 = sortedMap.entrySet(); 
      Iterator iterator2 = set2.iterator(); 
      System.out.println("Apr�s le tri: "); 
      int i =1; 
      while(iterator2.hasNext()) { 
         Map.Entry me2 = (Map.Entry)iterator2.next(); 
          
         if (verifDigit((String) me2.getValue()) !=true) { 
         tab[i] = (String) me2.getValue(); 
         i++;}} 
      String listeTrie ="";  
      for (i =tab.length-1; i>=0;i--) { 
    	  System.out.println(); 
    	  if (tab[i].contains(nomChamp) != true && tab[i].equals("") != true && verifDigit(tab[i]) != true) { 
    	  listeTrie = tab[i]+listeTrie; 
    	  System.out.println(tab[i]);} 
      } 
      listeTrie = infosFichier.split(";")[0]+";"+liste.get(nomChamp)+""+listeTrie; 
       
       
      try { 
			fw = new FileWriter (rb.getString("path")+nomFichier+".cda"); 
			bw = new BufferedWriter(fw); 
			bw.write(""+listeTrie); 
 
			 
			 
		} 
		catch (Exception e) { 
			 
		} 
		finally { 
			if (bw != null) { 
				try { 
					bw.close(); 
				} 
				catch (IOException e) { 
					e.printStackTrace(); 
				} 
			} 
		} 
       
       
} 
 
 
 
 
 
 
 
} 
 
// Select*From listePersonne Order by nom asc;