package fr.afpa.bean;

public class Table {
	
	private String nameTable;
	private int nbColumns;
	
	
	public Table(String nameTable, int nbColumns) {
		super();
		this.nameTable = nameTable;
		this.nbColumns = nbColumns;
	}


	public Table() {
		super();
	}


	public String getNameTable() {
		return nameTable;
	}


	public void setNameTable(String nameTable) {
		this.nameTable = nameTable;
	}


	public int getNbColumns() {
		return nbColumns;
	}


	public void setNbColumns(int nbColumns) {
		this.nbColumns = nbColumns;
	}
	
	
	
	

}
